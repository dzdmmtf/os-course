#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <time.h>
#include <iostream>
#include <sstream> //this header file is needed when using stringstream
#include <fstream>
#include <string>
#include <cstring>
#include <thread>
#include <semaphore.h>

#define MNIST_TESTING_SET_IMAGE_FILE_NAME "data/t10k-images-idx3-ubyte"  ///< MNIST image testing file in the data folder
#define MNIST_TESTING_SET_LABEL_FILE_NAME "data/t10k-labels-idx1-ubyte"  ///< MNIST label testing file in the data folder

#define HIDDEN_WEIGHTS_FILE "net_params/hidden_weights.txt"
#define HIDDEN_BIASES_FILE "net_params/hidden_biases.txt"
#define OUTPUT_WEIGHTS_FILE "net_params/out_weights.txt"
#define OUTPUT_BIASES_FILE "net_params/out_biases.txt"

#define NUMBER_OF_INPUT_CELLS 784   ///< use 28*28 input cells (= number of pixels per MNIST image)
#define NUMBER_OF_HIDDEN_CELLS 256   ///< use 256 hidden cells in one hidden layer
#define NUMBER_OF_OUTPUT_CELLS 10   ///< use 10 output cells to model 10 digits (0-9)

#define MNIST_MAX_TESTING_IMAGES 10000                      ///< number of images+labels in the TEST file/s
#define MNIST_IMG_WIDTH 28                                  ///< image width in pixel
#define MNIST_IMG_HEIGHT 28                                 ///< image height in pixel

using namespace std;


typedef struct MNIST_ImageFileHeader MNIST_ImageFileHeader;
typedef struct MNIST_LabelFileHeader MNIST_LabelFileHeader;

typedef struct MNIST_Image MNIST_Image;
typedef uint8_t MNIST_Label;
typedef struct Hidden_Node Hidden_Node;
typedef struct Output_Node Output_Node;
vector<Hidden_Node> hidden_nodes(NUMBER_OF_HIDDEN_CELLS);
vector<Output_Node> output_nodes(NUMBER_OF_OUTPUT_CELLS);

int errCount;
sem_t input_sem;
sem_t hidden_sem;
sem_t out_sem;
sem_t predict_sem;
sem_t input_mutex;
sem_t predict_mutex;

struct Hidden_Node{
    double weights[28*28];
    double bias;
    double output;
};

struct Output_Node{
    double weights[256];
    double bias;
    double output;
};

struct MNIST_Image{
    uint8_t pixel[28*28];
};

struct MNIST_ImageFileHeader{
    uint32_t magicNumber;
    uint32_t maxImages;
    uint32_t imgWidth;
    uint32_t imgHeight;
};

struct MNIST_LabelFileHeader{
    uint32_t magicNumber;
    uint32_t maxImages;
};

void locateCursor(const int row, const int col){
    printf("%c[%d;%dH",27,row,col);
}

void clearScreen(){
    printf("\e[1;1H\e[2J");
}

void displayImage(MNIST_Image *img, int row, int col){

    char imgStr[(MNIST_IMG_HEIGHT * MNIST_IMG_WIDTH)+((col+1)*MNIST_IMG_HEIGHT)+1];
    strcpy(imgStr, "");

    for (int y=0; y<MNIST_IMG_HEIGHT; y++){

        for (int o=0; o<col-2; o++) strcat(imgStr," ");
        strcat(imgStr,"|");

        for (int x=0; x<MNIST_IMG_WIDTH; x++){
            strcat(imgStr, img->pixel[y*MNIST_IMG_HEIGHT+x] ? "X" : "." );
        }
        strcat(imgStr,"\n");
    }

    if (col!=0 && row!=0) locateCursor(row, 0);
    printf("%s",imgStr);
}

void displayImageFrame(int row, int col){

    if (col!=0 && row!=0) locateCursor(row, col);

    printf("------------------------------\n");

    for (int i=0; i<MNIST_IMG_HEIGHT; i++){
        for (int o=0; o<col-1; o++) printf(" ");
        printf("|                            |\n");
    }

    for (int o=0; o<col-1; o++) printf(" ");
    printf("------------------------------");

}

void displayLoadingProgressTesting(int imgCount, int y, int x){

    float progress = (float)(imgCount+1)/(float)(MNIST_MAX_TESTING_IMAGES)*100;

    if (x!=0 && y!=0) locateCursor(y, x);

    printf("Testing image No. %5d of %5d images [%d%%]\n                                  ",(imgCount+1),MNIST_MAX_TESTING_IMAGES,(int)progress);

}

void displayProgress(int imgCount, int errCount, int y, int x){

    double successRate = 1 - ((double)errCount/(double)(imgCount+1));

    if (x!=0 && y!=0) locateCursor(y, x);

    printf("Result: Correct=%5d  Incorrect=%5d  Success-Rate= %5.2f%% \n",imgCount+1-errCount, errCount, successRate*100);


}

uint32_t flipBytes(uint32_t n){

    uint32_t b0,b1,b2,b3;

    b0 = (n & 0x000000ff) <<  24u;
    b1 = (n & 0x0000ff00) <<   8u;
    b2 = (n & 0x00ff0000) >>   8u;
    b3 = (n & 0xff000000) >>  24u;

    return (b0 | b1 | b2 | b3);

}

void readImageFileHeader(FILE *imageFile, MNIST_ImageFileHeader *ifh){

    ifh->magicNumber =0;
    ifh->maxImages   =0;
    ifh->imgWidth    =0;
    ifh->imgHeight   =0;

    fread(&ifh->magicNumber, 4, 1, imageFile);
    ifh->magicNumber = flipBytes(ifh->magicNumber);

    fread(&ifh->maxImages, 4, 1, imageFile);
    ifh->maxImages = flipBytes(ifh->maxImages);

    fread(&ifh->imgWidth, 4, 1, imageFile);
    ifh->imgWidth = flipBytes(ifh->imgWidth);

    fread(&ifh->imgHeight, 4, 1, imageFile);
    ifh->imgHeight = flipBytes(ifh->imgHeight);
}

void readLabelFileHeader(FILE *imageFile, MNIST_LabelFileHeader *lfh){

    lfh->magicNumber =0;
    lfh->maxImages   =0;

    fread(&lfh->magicNumber, 4, 1, imageFile);
    lfh->magicNumber = flipBytes(lfh->magicNumber);

    fread(&lfh->maxImages, 4, 1, imageFile);
    lfh->maxImages = flipBytes(lfh->maxImages);

}

FILE *openMNISTImageFile(char *fileName){

    FILE *imageFile;
    imageFile = fopen (fileName, "rb");
    if (imageFile == NULL) {
        printf("Abort! Could not fine MNIST IMAGE file: %s\n",fileName);
        exit(0);
    }

    MNIST_ImageFileHeader imageFileHeader;
    readImageFileHeader(imageFile, &imageFileHeader);

    return imageFile;
}

FILE *openMNISTLabelFile(char *fileName){

    FILE *labelFile;
    labelFile = fopen (fileName, "rb");
    if (labelFile == NULL) {
        printf("Abort! Could not find MNIST LABEL file: %s\n",fileName);
        exit(0);
    }

    MNIST_LabelFileHeader labelFileHeader;
    readLabelFileHeader(labelFile, &labelFileHeader);

    return labelFile;
}

MNIST_Image getImage(FILE *imageFile){

    MNIST_Image img;
    size_t result;
    result = fread(&img, sizeof(img), 1, imageFile);
    if (result!=1) {
        printf("\nError when reading IMAGE file! Abort!\n");
        exit(1);
    }

    return img;
}

MNIST_Label getLabel(FILE *labelFile){

    MNIST_Label lbl;
    size_t result;
    result = fread(&lbl, sizeof(lbl), 1, labelFile);
    if (result!=1) {
        printf("\nError when reading LABEL file! Abort!\n");
        exit(1);
    }

    return lbl;
}

void allocateHiddenParameters(){
    int idx = 0;
    int bidx = 0;
    ifstream weights(HIDDEN_WEIGHTS_FILE);
    for(string line; getline(weights, line); )   //read stream line by line
    {
        stringstream in(line);
        for (int i = 0; i < 28*28; ++i){
            in >> hidden_nodes[idx].weights[i];
      }
      idx++;
    }
    weights.close();

    ifstream biases(OUTPUT_BIASES_FILE);
    for(string line; getline(biases, line); )   //read stream line by line
    {
        stringstream in(line);
        in >> hidden_nodes[bidx].bias;
        bidx++;
    }
    biases.close();

}

void allocateOutputParameters(){
    int idx = 0;
    int bidx = 0;
    ifstream weights(OUTPUT_WEIGHTS_FILE); //"layersinfo.txt"
    for(string line; getline(weights, line); )   //read stream line by line
    {
        stringstream in(line);
        for (int i = 0; i < 256; ++i){
            in >> output_nodes[idx].weights[i];
      }
      idx++;
    }
    weights.close();

    ifstream biases(OUTPUT_BIASES_FILE);
    for(string line; getline(biases, line); )   //read stream line by line
    {
        stringstream in(line);
        in >> output_nodes[bidx].bias;
        bidx++;
    }
    biases.close();

}

int getNNPrediction(){

    double maxOut = 0;
    int maxInd = 0;

    for (int i=0; i<NUMBER_OF_OUTPUT_CELLS; i++){

        if (output_nodes[i].output > maxOut){
            maxOut = output_nodes[i].output;
            maxInd = i;
        }
    }

    return maxInd;

}

MNIST_Image img;

void output_func(int index, int n, sem_t* hiddens, sem_t* outs)
{
    for (int imgCount=0; imgCount<MNIST_MAX_TESTING_IMAGES; imgCount++) {
        for (int i = 0; i < n; ++i) {
            sem_wait(outs+index);
        }
        sem_wait(&out_sem);
        output_nodes[index].output = 0;
        for (int j = 0; j < NUMBER_OF_HIDDEN_CELLS; j++) {
            output_nodes[index].output += hidden_nodes[j].output * output_nodes[index].weights[j];
        }
        output_nodes[index].output += output_nodes[index].bias;
        output_nodes[index].output += 1/(1+ exp(-1* output_nodes[index].output));
        for (int i = 0; i < n; ++i) {
            sem_post(hiddens+i);
        }
        sem_post(&predict_sem);
    }
}

void hidden_func(int index, sem_t* hiddens, int n, sem_t* outs)
{
    for (int imgCount=0; imgCount<MNIST_MAX_TESTING_IMAGES; imgCount++) {
        for (int i = 0; i < 10; ++i) {
            sem_wait(hiddens+(index/(256/n)));
        }
        sem_wait(&hidden_sem);

        for (int i = 0; i < 256/n; ++i) {
            hidden_nodes[index+i].output = 0;
            for (int z = 0; z < NUMBER_OF_INPUT_CELLS; z++) {
                hidden_nodes[index+i].output += img.pixel[z] * hidden_nodes[index+i].weights[z];
            }
            hidden_nodes[index+i].output += hidden_nodes[index+i].bias;
            hidden_nodes[index+i].output = (hidden_nodes[index+i].output >= 0) ?  hidden_nodes[index+i].output : 0; //relu
        }  
        
        sem_post(&input_sem);
        
        for (int i = 0; i < 10; ++i) {
            sem_post(outs+i);
        }
    }
}

void input_func(int n, FILE *imageFile)
{
    for (int imgCount=0; imgCount<MNIST_MAX_TESTING_IMAGES; imgCount++) {
        for (int i = 0; i < n; i++) {
            sem_wait(&input_sem);
        }
        
        sem_wait(&input_mutex);
        displayLoadingProgressTesting(imgCount,5,5);
        img = getImage(imageFile);
        displayImage(&img, 8,6);
        sem_post(&predict_mutex);
        for (int i = 0; i < n; i++) {
            sem_post(&hidden_sem); 
        }
    }
}

void predict_func(FILE *labelFile)
{
    for (int imgCount=0; imgCount<MNIST_MAX_TESTING_IMAGES; imgCount++){
        for (int i = 0; i < 10; ++i) {
            sem_wait(&predict_sem);
        }
        MNIST_Label lbl = getLabel(labelFile);
        int predictedNum = getNNPrediction();
        if (predictedNum!=lbl) errCount++;
        printf("\n      Prediction: %d   Actual: %d ",predictedNum, lbl);
        sem_wait(&predict_mutex);
        displayProgress(imgCount, errCount, 5, 66);
        sem_post(&input_mutex);
        for (int i = 0; i < 10; ++i) {
            sem_post(&out_sem);
        }
    }
   
}

void testNN(){
    
    int n;
    cout<<"enter number of hidden threads: "<<endl;
    cin>>n;

    FILE *imageFile, *labelFile;
    imageFile = openMNISTImageFile(MNIST_TESTING_SET_IMAGE_FILE_NAME);
    labelFile = openMNISTLabelFile(MNIST_TESTING_SET_LABEL_FILE_NAME);

    displayImageFrame(7,5);
    errCount = 0;
    
    sem_t* hiddens = new sem_t[n];
    sem_t* outs = new sem_t[10];

    sem_init(&input_sem, 0, n);
    sem_init(&hidden_sem, 0, 0); 
    sem_init(&out_sem, 0, 10);
    sem_init(&predict_sem, 0, 0);
    sem_init(&input_mutex, 0 , 1);
    sem_init(&predict_mutex, 0, 0);
    for (int i = 0; i < n; ++i) {
        sem_t tmp;
        sem_init(&tmp, 0, 10);
        hiddens[i] = tmp;
    }
    for (int i = 0; i < 10; ++i) {
        sem_t tmp;
        sem_init(&tmp, 0, 0);
        outs[i] = tmp;
    } 


    //*******INPUT LAYER********//
    thread input_thread(input_func, n, imageFile);
    //*******HIDDEN LAYER*******//
    thread hidden_threads[n];
    int hidden_index = 0;
    for (int i = 0; i < n; ++i) {
        hidden_threads[i] = thread(hidden_func, hidden_index, hiddens, n, outs);
        hidden_index += 256/n;
    }    
    //*******OUTPUT LAYER*******//
    thread output_threads[10];
    int output_index = 0;
    for (int i = 0; i < 10; ++i) {
        output_threads[i] = thread(output_func, output_index, n, hiddens, outs);
        output_index++;
    }
    //*******PREDICT LAYER*******///
    thread predict_thread(predict_func, labelFile);



    //**********JOIN************//
    input_thread.join();
    for (int i = 0; i < n; ++i) {
        hidden_threads[i].join();
    }
    for (int i = 0; i < 10; ++i) {
        output_threads[i].join();
    }
    predict_thread.join();

    fclose(imageFile);
    fclose(labelFile);
}

int main(int argc, const char * argv[]) {

    time_t startTime = time(NULL);

    clearScreen();
    printf("    MNIST-NN: a simple 2-layer neural network processing the MNIST handwriting images\n");

    allocateHiddenParameters();
    allocateOutputParameters();

    testNN();

    locateCursor(38, 5);

    time_t endTime = time(NULL);
    double executionTime = difftime(endTime, startTime);
    printf("\n    DONE! Total execution time: %.1f sec\n\n",executionTime);

    return 0;
}
