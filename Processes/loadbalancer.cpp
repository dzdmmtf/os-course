#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <sstream>
#include <sys/types.h> 
#include <unistd.h>
#include <stdlib.h> 
#include <sys/wait.h> 
#include "dirent.h"
#include <fcntl.h> 
#include <sys/stat.h> 

#define MSGSIZE 50

using namespace std;

vector<string> getFilenames(const char* d)
{
	DIR *dir;
	struct dirent *ent;
	vector <string> filenames;
	if ((dir = opendir (d)) != NULL) {
	  while ((ent = readdir (dir)) != NULL) {
	  	if(strcmp(ent->d_name, ".")!=0 && strcmp(ent->d_name, "..")!=0)
		  	filenames.push_back(ent->d_name);
	  }
	  closedir (dir);
	}
	return filenames;
}

vector<string> tokenize_input(string input)
{
	int start = 0;
	string token;
	vector<string> tokens;
	for (int i = 0; i < input.length(); ++i) {
		if (input[i]=='-' ){
			token = input.substr(start, i-start);
			start = i+1;
			for (int j = 0; j < token.length(); ++j) {
				if(token[j]=='=') {
					tokens.push_back(token.substr(1, j-1));
					tokens.push_back(token.substr(j+2, token.length()-j-2));
				}
			}
		}
		else if(i==input.length()-1){
			token = input.substr(start, i-start+1);
			for (int j = 0; j < token.length(); ++j) {
				if(token[j]=='=') {
					tokens.push_back(token.substr(1, j-1));
					tokens.push_back(token.substr(j+2, token.length()-j-2));
				}
			}
		}
	}
	return tokens;
}

void handle_input(string input, int& prc_cnt, int& sort_wanted, int& is_ascend, string &sorting_val, vector<string> &tokens, vector<string> &filenames, vector<string> &filter_items) 
{
	const char* dir;
	tokens = tokenize_input(input);

    stringstream x(tokens[tokens.size()-3]);
    x >> prc_cnt;

	dir = tokens[tokens.size()-1].c_str();
	filenames = getFilenames(dir);
	
	
	if(tokens.size() > 4) {
		if(tokens[tokens.size()-5]=="ascend " || tokens[tokens.size()-5]=="descend ") {
			sort_wanted = 1;
		if(tokens[tokens.size()-5]=="ascend ")
			is_ascend = 1;
		}
	}

	if(sort_wanted) {
		sorting_val = tokens[tokens.size()-6];
		for(int i=0; i<(tokens.size()-6) ;i++)
			filter_items.push_back(tokens[i]);
	}
	else {
		for(int i=0; i<(tokens.size()-4) ;i++)
			filter_items.push_back(tokens[i]);
	}
}

void attach_filters(vector <string> filter_items, string &filter_string)
{
	for (int i = 0; i < filter_items.size(); ++i) {
		filter_string += filter_items[i].substr(0,filter_items[i].length()-1);
		if(i!=filter_items.size()-1)
			filter_string += ",";
	}
}

void load_balancer() {
	while(true){
		string input=" ", tmp;
		getline(cin, tmp);
		if(tmp=="quit")
			break;
		input += tmp;
		int prc_cnt, files_num, constant, diff;
		int sort_wanted = 0;
		int is_ascend = 0;
		char dir[20];

		string sorting_val = "";
		string filter_string;
		vector<string> tokens;
		vector<string> filenames;
		vector<string> filter_items;
		vector<int> pipes;
		vector<int> pids;

		handle_input(input, prc_cnt, sort_wanted, is_ascend, sorting_val, tokens, filenames, filter_items);
		files_num = filenames.size();
		constant = files_num/prc_cnt;
		diff = files_num%prc_cnt;
		strcpy(dir, tokens[tokens.size()-1].c_str());
		attach_filters(filter_items, filter_string);	

		//**************presenter****************//
	    int presenter_pid = fork();
	    if(presenter_pid==0){
	    	char executable[12] = "./presenter";
	    	char *args[] = {NULL};
	    	execv(executable, args);
	    	exit(0);
	    }

		//**************worker********************//
		for(int i=0;i<prc_cnt;i++) { 
	    	int p[2];
			if (pipe(p) < 0)	exit(1);
	    	int pid = fork();
	    	
	    	if(pid==0) {
	    		char p0[1];
	    		char filter[100];
	    		strcpy(filter, filter_string.c_str());
	    		char char_descriptor = '0' + p[0];
	        	p0[0] = char_descriptor;
	        	char executable[9] = "./worker";
	        	char *args[] = {p0, filter, dir, NULL};
	        	execv(executable,args);
	        	exit(0);
	    	}
	    	else {
	    		pipes.push_back(p[1]);
	    		pids.push_back(pid);
	    	}
	    }

	    //pipe between loadbalancer and worker
	    int filename_index = 0;
	    for (int i = 0; i < pipes.size(); ++i) {
	    	string char_filename;
	    	if(i<diff) {
				for (int j = 0; j < constant+1; ++j){
					char_filename += filenames[filename_index];
					if(j!=constant)
						char_filename += "-";
					filename_index++;
				}
	    		write(pipes[i], char_filename.c_str(), MSGSIZE);
			}
			else {
				for (int j = 0; j < constant; ++j){
					char_filename += filenames[filename_index];
					if(j!=constant-1)
						char_filename += "-";
					filename_index++;
				}
	    		write(pipes[i], char_filename.c_str(), MSGSIZE);
			}
	    }

	    //pipe between loadbalancer and presenter

		string asc;
		stringstream aa;
		aa << is_ascend;
		asc = aa.str();

		string sorted;
		stringstream bb;
		bb << sort_wanted;
		sorted = bb.str();

		string workers;
		stringstream cc;
		cc << prc_cnt;
		workers = cc.str();

		sorting_val = sorting_val.substr(0,sorting_val.length()-1);
		
		string from_loadbalancer_to_presenter = "@";
		from_loadbalancer_to_presenter += workers + asc + sorted + sorting_val + "@";
		
		int fd; 
		string file_path = "myfifo";
		const char* myfifo = file_path.c_str(); 
		mkfifo(myfifo, 0666);

	    fd = open(myfifo, O_WRONLY);
	    write(fd, from_loadbalancer_to_presenter.c_str(), strlen(from_loadbalancer_to_presenter.c_str()));

	    for (int i = 0; i < pids.size(); ++i) {
		    int status;
		    while (-1 == waitpid(pids[i], &status, 0));
		    if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) {
		        exit(1);
		    }
		}
	    wait(&presenter_pid);
	}

}
	
int main(int argc, char const *argv[]) {
	load_balancer();
	
	return 0;
}