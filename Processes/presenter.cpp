#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <sys/types.h> 
#include <unistd.h>
#include <stdlib.h> 
#include <sys/wait.h> 
#include <fstream>
#include <fcntl.h> 
#include <sys/stat.h> 

using namespace std;

void split(string worker_output, vector<string> &splited_worker_output)
{
	int prev_dash = 0;
	
	for (int i = 0; i < worker_output.length(); ++i){
    	if(worker_output[i]=='-'){
    		splited_worker_output.push_back(worker_output.substr(prev_dash, i-prev_dash));
    		prev_dash = i+1;
    	}
    	else if(i==worker_output.length()-1)
    		splited_worker_output.push_back(worker_output.substr(prev_dash, i-prev_dash+1));
    }
}

void swap(string *xp, string *yp) 
{ 
    string temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
} 

void merge_sort(vector<string> input, vector<string> &output, string ascend, vector<vector<string> >input_words,int index)
{
	if(output.size()==0){
		for (int i = 0; i < input.size(); ++i){
			output.push_back(input[i]);
		}	
	}
	else{
		vector<string> middle(input.size()+output.size());
		vector<vector<string > >output_words;
		vector<string> tmp;
		for (int i = 0; i < output.size(); ++i){
			int prev_space = 0;
			for (int j = 0; j < output[i].length(); ++j){
				if(isspace(output[i][j])){
	    			tmp.push_back(output[i].substr(prev_space, j-prev_space));
	    			prev_space = j+1;
	    		}
	    		else if(j==output[i].length()-1){
	    			tmp.push_back(output[i].substr(prev_space, j-prev_space+1));		
	    		}
			}
			
	 		output_words.push_back(tmp);
	 		tmp.clear();
		}
		int input_index = 0;
		int output_index = 0;
		int k = 0;
		while(input_index<input.size() && output_index<output.size()){
			int a = atoi(input_words[input_index][index].c_str());
			int b = atoi(output_words[output_index][index].c_str());
			if(ascend=="1"){
				if(a < b)
					middle[k++] = input[input_index++];
				else
					middle[k++] = output[output_index++];
			}
			else{
				if(a < b)
					middle[k++] = output[output_index++];
				else
					middle[k++] = input[input_index++];
			}	
		}
		while(input_index<input.size())
			middle[k++] = input[input_index++];
		while(output_index<output.size()){
			middle[k++] = output[output_index++];
		}
		output.clear();
		for (int i = 0; i < middle.size(); ++i){
			output.push_back(middle[i]);
		}
	}
}

void sort(vector<string> &splited_worker_output, vector<char> loadbalancer_message, vector<string> &output)
{
	string sort_message(loadbalancer_message.begin(), loadbalancer_message.end());
	string ascend = sort_message.substr(1,1);
	string sorted = sort_message.substr(2,1);
	string sort_val = sort_message.substr(3,sort_message.length()-4);
	int index;
	
	vector<vector<string> > words;
	vector<string> tmp;
	for (int i = 0; i < splited_worker_output.size(); ++i){
		int prev_space = 0;
		for (int j = 0; j < splited_worker_output[i].length(); ++j){
			if(isspace(splited_worker_output[i][j])){
    			tmp.push_back(splited_worker_output[i].substr(prev_space, j-prev_space));
    			prev_space = j+1;
    		}
    		else if(j==splited_worker_output[i].length()-1){
    			tmp.push_back(splited_worker_output[i].substr(prev_space, j-prev_space+1));		
    		}
		}
		
 		words.push_back(tmp);
 		tmp.clear();
	}
	//find index of sorting value
	for (int i = 0; i < words[0].size(); ++i)
		if (sort_val==words[0][i])
			index = i;
	//delete first lines
	for (int i = 0; i < words.size(); ++i){
		if(words[i][index]==sort_val){
			words.erase(words.begin()+i);
			splited_worker_output.erase(splited_worker_output.begin()+i);
		}
	}
		
	if(sorted=="1"){
		if(ascend=="1"){
			//ascending bubble sort
			for (int i = 0; i < splited_worker_output.size()-1; ++i){
				for (int j = 0; j < splited_worker_output.size()-i-1; j++) {
					int a = atoi(words[j][index].c_str());
					int b = atoi(words[j+1][index].c_str());
					if(a>b){
						swap(&splited_worker_output[j], &splited_worker_output[j+1]);
						words[j].swap(words[j+1]);
					}
			    }
			}
		}

		else{
			//descending bubble sort
			for (int i = 0; i < splited_worker_output.size()-1; ++i){
				for (int j = 0; j < splited_worker_output.size()-i-1; j++) {
					int a = atoi(words[j][index].c_str());
					int b = atoi(words[j+1][index].c_str());
					if(a<b){
						swap(&splited_worker_output[j], &splited_worker_output[j+1]);
						words[j].swap(words[j+1]);
					}
			    }
			}
		}
		merge_sort(splited_worker_output, output, ascend, words, index);
	}
	else{
		//doesn't need sort
		splited_worker_output.erase(splited_worker_output.begin());
		for (int i = 0; i < splited_worker_output.size(); ++i){
			output.push_back(splited_worker_output[i]);
		}
	}
}

int main(int argc, char *argv[])
{	
	int fd1; 
	bool loadbalancer_begin = false;
	bool loadbalancer_end = false;
	bool worker_begin = false;
	bool worker_end = false;
	vector<char> loadbalancer_message;
	vector <char> worker_message;
	vector<string> splited_worker_output;
	vector<string> output;
    string file_path = "myfifo";
    const char* myfifo = file_path.c_str(); 
    mkfifo(myfifo, 0666); 
    fd1 = open(myfifo,O_RDONLY);

    int count = 0;
    while(true){
    	char one_byte[1];
		read(fd1, one_byte, 1);
		if(one_byte[0]=='@' && loadbalancer_begin==false){
			loadbalancer_begin = true;
			continue;
		}
		if(loadbalancer_begin==true && loadbalancer_end==false)
			loadbalancer_message.push_back(one_byte[0]);
		if(one_byte[0]=='@' && loadbalancer_begin==true){
			loadbalancer_end = true;
			continue;
		}

		if(one_byte[0]=='#' && worker_begin==false){
			worker_begin = true;
			continue;
		}
		if(worker_begin==true && worker_end==false){
			worker_message.push_back(one_byte[0]);
		}
		if(one_byte[0]=='#' && worker_begin==true){
			worker_end = true;
			worker_message.pop_back();
			string worker_output(worker_message.begin(), worker_message.end());
			split(worker_output, splited_worker_output);
			sort(splited_worker_output, loadbalancer_message, output);
			worker_begin = false;
			worker_end = false;
			worker_message.clear();
			splited_worker_output.clear();
			count++;
			if((count + '0')==((int)(loadbalancer_message[0]))){
				break;
			}
			else
				continue;
		}
    }
    for (int i = 0; i < output.size(); ++i)
		cout<<output[i]<<endl;
    close(fd1);
	return 0;
}