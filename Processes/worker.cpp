#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <vector>
#include <sys/types.h> 
#include <unistd.h>
#include <stdlib.h> 
#include <sys/wait.h> 
#include <fstream>
#include <fcntl.h> 
#include <sys/stat.h> 

#define MSGSIZE 50

using namespace std;

void get_first_line_items(vector <string> &first_line_items, vector <int> &index, string filename, string dir, vector <string> filter_items)
{
	int prev_space = 0;
	int line_count = 0;
	string path =  "./" + dir + "/" + filename;
	ifstream ifs(path.c_str());
	string line;
	while(getline(ifs, line)) {
		if(line_count==0){
			//get first line items
			for (int j = 0; j < line.length(); ++j){
				if(isspace(line[j])) {
					first_line_items.push_back(line.substr(prev_space, j-prev_space));
					prev_space = j+1;
				}
				else if(j==line.length()-1)
					first_line_items.push_back(line.substr(prev_space, j-prev_space+1));
			}
		}
		line_count++;
	}
	//find indexes
	for (int j = 0; j < filter_items.size(); j+=2){
		for (int k = 0; k < first_line_items.size(); ++k){
			if(first_line_items[k]==filter_items[j]){
				index.push_back(k);
			}
		}
	}	
}

void filter_files(vector<string> filenames, vector<string> filter_items, char* directory, vector<string> &filtered_lines)
{
	vector <string> first_line_items;
	vector <int> index;
	string dir(directory);
	get_first_line_items(first_line_items, index, filenames[0], dir, filter_items);
	for (int i = 0; i < filenames.size(); ++i){
    	
    	
    	int line_count = 0;
    	string path = "./" + dir + "/" + filenames[i];
    	ifstream myfile(path.c_str());
		string line;

		while(getline(myfile, line)){
			int k = 0;
			if(line_count==0)
				filtered_lines.push_back(line);
			else {
				vector <string> values;
				int prev = 0;
				//split values of each line
				for (int j = 0; j < line.length(); ++j){
					if(isspace(line[j])) {
						values.push_back(line.substr(prev, j-prev));
						prev = j+1;
					}
					else if(j==line.length()-1)
						values.push_back(line.substr(prev, j-prev+1));
				}
				for (int j = 0; j < filter_items.size(); j+=2){
					if(values[index[k]] == filter_items[j+1]){
						if(k!=index.size()-1){
							k++;
							continue;
						}
						else{
							k++;
							filtered_lines.push_back(line);
						}
					}
				}
			}
			line_count++;
		}
    }
}

void split_inputs(string tmp, string temp, vector<string> &filenames, vector<string> &filters)
{
	int prev_comma = 0;
    int prev_dash = 0;
	
	for (int i = 0; i < tmp.length(); ++i){
    	if(tmp[i]=='-'){
    		filenames.push_back(tmp.substr(prev_dash, i-prev_dash));
    		prev_dash = i+1;
    	}
    	else if(i==tmp.length()-1)
    		filenames.push_back(tmp.substr(prev_dash, i-prev_dash+1));
    }

    for (int i = 0; i < temp.length(); ++i){
    	if(temp[i]==','){
    		filters.push_back(temp.substr(prev_comma, i-prev_comma));
    		prev_comma = i+1;
    	}
    	else if(i==temp.length()-1)
    		filters.push_back(temp.substr(prev_comma, i-prev_comma+1));
    }
}

void concat_filtered_lines(vector<string> filtered_lines, string &presenter_input)
{
	for (int i = 0; i < filtered_lines.size(); ++i){
		presenter_input += filtered_lines[i];
		if(i!=filtered_lines.size()-1)
			presenter_input += "-";
		else
			presenter_input += "#";
	}
}

int main(int argc, char *argv[])
{
    vector <string> filenames;
    vector <string> filters;
    vector <string> filtered_lines;
    string presenter_input = "#";

	char inbuf[MSGSIZE];    
    int read_descriptor = atoi(argv[0]);
	char *filter = argv[1];
	char* dir = argv[2];

    read(read_descriptor, inbuf, MSGSIZE); 

    string tmp(inbuf);
    string temp(filter);
    split_inputs(tmp, temp, filenames, filters);
    filter_files(filenames, filters, dir, filtered_lines);
    concat_filtered_lines(filtered_lines, presenter_input);

    //named pipes
	int fd; 
	string file_path = "myfifo";
	const char* myfifo = file_path.c_str(); 
	mkfifo(myfifo, 0666); 

    fd = open(myfifo, O_WRONLY);
    write(fd, presenter_input.c_str(), strlen(presenter_input.c_str())); 
    
    close(fd); 
    
	return 0;
}